from django.contrib import admin
from .models import Post
# Register your models here.

# Adds models to your admin page
admin.site.register(Post)
