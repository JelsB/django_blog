from django.shortcuts import render, get_object_or_404
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.contrib.auth.models import User
from django.views.generic import (ListView, DetailView, CreateView, UpdateView,
                                  DeleteView)

from .models import Post
# Create your views here.


def home(request):
    # Able to use Queries on Models
    # objects are the ones that are present in the database
    context = {'posts': Post.objects.all()}
    return render(request, 'blog/home.html', context)

class PostListView(ListView):
    model = Post
    # override defaul template_name '<app>/<model>_<viewtype>.html'
    template_name = 'blog/home.html'
    context_object_name = 'posts'
    # newest post first
    ordering = ['-date_posted']
    paginate_by = 5

class UserPostListView(ListView):
    model = Post
    # override defaul template_name '<app>/<model>_<viewtype>.html'
    template_name = 'blog/user_posts.html'
    context_object_name = 'posts'
    # newest post first
    paginate_by = 5

    # override what to query
    def get_queryset(self):
        # name-based (self.kwargs) arguments captured according to the URLconf.
        user = get_object_or_404(User, username=self.kwargs.get('username'))
        return Post.objects.filter(author=user).order_by('-date_posted')

class PostDetailView(DetailView):
    model = Post

# LoginRequiredMixin as FIRST arg to unforce a user to be logged in before
# creating a new post
class PostCreateView(LoginRequiredMixin, CreateView):
    model = Post
    fields = ['title', 'content']
    # defaul template_name '<app>/<model>_form.html'

    # override form_valid method to specify current user as post author
    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)

# UserPassesTestMixin needed test is user passes test_func()
class PostUpdateView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = Post
    fields = ['title', 'content']
    # defaul template_name '<app>/<model>_form.html'
    # same as CreateView

    # override form_valid method to specify current user as post author
    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)

    # check if user is author of the post
    def test_func(self):
        post = self.get_object()
        if self.request.user == post.author:
            return True
        return False

class PostDeleteView(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
    model = Post
    # return to home after deletion
    success_url = '/'

    # check if user is author of the post
    def test_func(self):
        post = self.get_object()
        if self.request.user == post.author:
            return True
        return False

def about(request):

    return render(request, 'blog/about.html', {'title': 'About'})
