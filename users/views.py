from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from .forms import UserRegisterForm, UserUpdateForm, ProfileUpdateForm
# Create your views here.


def register(request):
    if request.method == 'POST':
        form = UserRegisterForm(request.POST)
        if form.is_valid():
            # this automatically saves the new User
            form.save()
            username = form.cleaned_data.get('username')
            messages.success(request, f'Account has been created! '
                             f'You can know login as {username}')
            return redirect('login')
    else:
        form = UserRegisterForm()
    return render(request, 'users/register.html', {'form': form})

@login_required
def profile(request):
    # happens when a form is submitted (posted)
    if request.method == 'POST':
        # Instantiate with current users info
        # This will fill in the fkorm with the current user's info
        # request.POST = POST data
        u_form = UserUpdateForm(request.POST, instance=request.user)
        p_form = ProfileUpdateForm(request.POST, request.FILES,
                                    instance=request.user.profile)
        # NOTE: When image file name already exists, a new one with
        #'filename'+ randomcombo is created.
        # TODO: remove previous image before saving new one
        # save when both forms are valid
        if u_form.is_valid() and p_form.is_valid():
            u_form.save()
            p_form.save()
            messages.success(request, f'Account has been updated!')
            # redirect instead of end render to avoid 'reload message'
            return redirect('profile')
    else:
        u_form = UserUpdateForm(instance=request.user)
        p_form = ProfileUpdateForm(instance=request.user.profile)

    context = {'u_form': u_form, 'p_form': p_form}

    return render(request, 'users/profile.html', context)
